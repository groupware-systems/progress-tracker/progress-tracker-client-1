/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of Progress Tracker Client 1.
 *
 * Progress Tracker Client 1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * Progress Tracker Client 1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Progress Tracker Client 1. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var xmlhttp = null;

// Mozilla
if (window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
// IE
else if (window.ActiveXObject)
{
    xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
}

if (xmlhttp == null)
{
    console.log("No AJAX.");
}

var basePath = "";

{
    var scripts = document.getElementsByTagName("script");
    // 'scripts' contains always the last script tag that was loaded.
    var myPath = scripts[scripts.length-1].src;

    basePath = myPath.substring(0, myPath.lastIndexOf('/'));
}

// 'file://' is bad.
if (basePath.substring(0, 7) == "file://")
{
    basePath = basePath.substr(8);
    basePath = "https://" + basePath;
}



let projects = null;

function loadProjects()
{
    projects = new ProjectList();
    projects.requestProjects();
}

function ProjectList()
{
    let that = this;

    that.projects = new Array();

    that.requestProjects = function()
    {
        that.projects = new Array();

        xmlhttp.open("GET", basePath + "/api/project.php", true);
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.onreadystatechange = _resultRequestProjects;
        xmlhttp.send();
    };

    function _resultRequestProjects()
    {
        if (xmlhttp.readyState != 4)
        {
            // Waiting...
            return 0;
        }

        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            let result = JSON.parse(xmlhttp.responseText);
            result = result.project;

            for (let i = 0, max = result.length; i < max; i++)
            {
                that.projects.push(new Project(result[i].id, result[i].title));
            }
        }

        _renderProjects();
    }

    function _renderProjects()
    {
        let target = document.getElementById("projects");

        if (target == null)
        {

        }

        while (target.hasChildNodes() == true)
        {
            target.removeChild(target.lastChild);
        }


        let row = null;

        for (let i = 0, max = that.projects.length; i < max; i++)
        {
            if (row == null)
            {
                row = document.createElement("div");
                row.classList.add("row");
            }

            let cell = document.createElement("div");
            cell.classList.add("span4");

            let heading = document.createElement("h2");
            heading.innerText = that.projects[i].title;
            cell.appendChild(heading);

            row.appendChild(cell);

            if (i > 0 && (i + 1) % 3 == 0)
            {
                target.appendChild(row);
                row = null;
            }
        }

        if (row == null)
        {
            row = document.createElement("div");
            row.classList.add("row");
        }

        {
            let cell = document.createElement("div");
            cell.classList.add("span4");

            let form = document.createElement("form");

            let container = document.createElement("div");
            container.classList.add("form-group");

            let labelTitle = document.createElement("label");
            labelTitle.setAttribute("for", "title");
            labelTitle.innerText = "Title";
            container.appendChild(labelTitle);

            let controlTitle = document.createElement("input");
            controlTitle.classList.add("form-control");
            controlTitle.setAttribute("type", "text");
            controlTitle.setAttribute("minlength", "1");
            controlTitle.setAttribute("id", "title");
            controlTitle.setAttribute("required", "required");
            container.appendChild(controlTitle);

            form.appendChild(container);

            let buttonSubmit = document.createElement("button");
            buttonSubmit.setAttribute("type", "button");
            buttonSubmit.setAttribute("onclick", "projects.addProject();");
            buttonSubmit.classList.add("btn");
            buttonSubmit.classList.add("btn-primary");
            buttonSubmit.innerText = "Add";
            form.appendChild(buttonSubmit);

            cell.appendChild(form);

            row.appendChild(cell);
        }

        if (row != null)
        {
            target.appendChild(row);
        }
    }

    that.addProject = function()
    {
        let controlTitle = document.getElementById("title");

        if (controlTitle == null)
        {

        }

        let title = controlTitle.value;

        if (title.length <= 0)
        {
            /** @todo Not known why this doesn't work/show. */
            controlTitle.classList.add("is-invalid");
            return 1;
        }

        xmlhttp.open("POST", basePath + "/api/project.php", true);
        xmlhttp.setRequestHeader("Content-Type", "application/json");
        xmlhttp.onreadystatechange = _resultAddProject;
        xmlhttp.send("{\"title\":" + JSON.stringify(title) + "}");

        controlTitle.classList.remove("is-invalid");
    };

    function _resultAddProject()
    {
        if (xmlhttp.readyState != 4)
        {
            // Waiting...
            return 0;
        }

        if (xmlhttp.readyState == 4 && xmlhttp.status == 201)
        {
            let result = JSON.parse(xmlhttp.responseText);

            that.projects.push(new Project(result.id, result.title));

            _renderProjects();
        }
    }
}

function Project(id, title)
{
    let that = this;

    that.id = id;
    that.title = title;
}
